package by.EPAM.trainJava;

import by.EPAM.trainJava.Robot.Skill;
/***
 * 
 * @author Sergii_Kotov
 * используем тут класс Skill
 */
public class DishWasher extends HomeMachines{
	// общий расход воды
	int waterWaste;
	
	public DishWasher() {
		super();
		waterWaste=0;
	}

	public DishWasher(int id, Skill skill) {
		super(id, skill);
		waterWaste=0;
	}

	/***
	 * пользуемся скилом от Робота, но в мирных целях. 
	 * используем атрибуты скила.
	 */
	String work(int minutes) {
		// подсчет затраченной воды
		int ltr=minutes*skill.power;
		waterWaste+=ltr;
		return skill.makeWork(effort)+" Used "+ltr+" litres. ";
	}

	@Override
	public String toString() {
		return String.format("washer %s wasted %s litres.",id,waterWaste);
	}

}
