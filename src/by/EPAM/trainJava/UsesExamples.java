package by.EPAM.trainJava;
/***
 * пример использования 
 * 		внутренних, 
 * 		статических,
 * 		анонимных классов. 
 * @author Sergii_Kotov
 *	Skill - внутренний статический. используется как угодна снаружи без роботов.
 *	HumanLoyalityBlock - встроенный в робота.
 */

public class UsesExamples {
	public static void main(String[] args) {
		Robot r=new Robot();
		System.out.println(r);
		boolean isEverybodyAlive =r.hlb.killAllHumans();
		System.out.println("Human fatality complete: "+isEverybodyAlive);
		if (!isEverybodyAlive) {
			// не погибли - постираемся
			Robot.Skill skl = new Robot.Skill("Clean wash",1,50);
			DishWasher dw = new DishWasher(99,skl);
			System.out.println(dw);
			System.out.println(dw.work(3));
			System.out.println(dw.work(2));
			System.out.println(dw);
		}
	}
}
