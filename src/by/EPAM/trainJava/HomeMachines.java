package by.EPAM.trainJava;

import by.EPAM.trainJava.Robot.Skill;

public abstract class HomeMachines {
	static final int effort=10;
	int id;
	Robot.Skill skill; 
	abstract String work(int minutes);

	public HomeMachines() {
		this.id = 0;
		this.skill = new Robot.Skill();
	}
	
	public HomeMachines(int id, Skill skill) {
		this.id = id;
		this.skill = skill;
	}
	
}
