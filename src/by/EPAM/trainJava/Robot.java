package by.EPAM.trainJava;

import java.util.ArrayList;
import java.util.List;

/***
 * пример использования 
 * 		внутренних, 
 * 		статических,
 * 		анонимных классов. 
 * @author Sergii_Kotov
 *
 */
public class Robot {
	String name;
	String model;
	HumanLoyalityBlock hlb;
	List <Skill> skills;
	
	public Robot(){
		this.name = "term1";
		this.model = "T100";
		ArrayList <Skill> terminatorSkill = new ArrayList <Skill>();
		// анонимный класс при создании дефолтного робота
		// наделяем его нестандартными скилами 
		terminatorSkill.add(new Skill ("Murder",100,1000){
			@Override 
			String makeWork(int effort) {
				return super.makeWork(effort)+" ha-ha-ha!";
			}
		});
		this.skills= terminatorSkill;
		hlb=new HumanLoyalityBlock(0,10);
		// сразу начинаем делать смерть человекам
		hlb.killAllHumans();
	}
	
	public Robot(String name, String model, List <Skill> skills, HumanLoyalityBlock hlb) {
		this.name = name;
		this.model = model;
		this.skills=skills;
		this.hlb=hlb;
	}

	/***
	 * вложенный класс Inner class
	 * нужен только для робота, нигде больше не применится
	 */
	class HumanLoyalityBlock {
		int kindness;
		int stupiedness;
		
		public HumanLoyalityBlock() {
			kindness=0;
			stupiedness=0;
		}
		
		public HumanLoyalityBlock(int kindness, int stupiedness) {
			this.kindness = kindness;
			this.stupiedness = stupiedness;
		}

		boolean killAllHumans() {
			if ((kindness<1000) || (stupiedness>kindness)) {
				if (model.equals("T1000")){
					return true;
				} else {
					return (Math.random()>0.5); 
				}
			} else return false;
		}
		@Override
		public String toString() {
			return String.format("loyalty (%3s;%3s)",kindness,stupiedness);
		}
	}
	
	/**
	 * Nested class
	 * Класс Навык
	 * он не связан жестко с роботами, так как навыки применяются много где. 
	 * например у бытовых приборов -  класс HomeMachines 
	 */
	static class Skill {
		String name;
		int speed;
		int power;
		
		public Skill() {
			this.name = "useless";
			this.speed = 0;
			this.power = 0;
		}
		
		public Skill(String name, int speed, int power) {
			this.name = name;
			this.speed = speed;
			this.power = power;
		}

		/***
		 * выполнить некую работу согласно навыку. 
		 * @param effort степень тяжести работы
		 * @return	выполнена или нет - строковое сообщение
		 */
		String makeWork(int effort){
			if (effort>power) {
				return "Can't do it for you, master, It's too hard!";
			}
			return "I've done "+name + " on speed "+ speed;
		}
		
		@Override
		public String toString() {
			return String.format("Skill %s with power %s",name,power);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public HumanLoyalityBlock getHlb() {
		return hlb;
	}

	public void setHlb(HumanLoyalityBlock hlb) {
		this.hlb = hlb;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}


	@Override
	public String toString() {
		return String.format("%25s%25s%55s%55s\n",name,model,hlb,skills);
	}

	// ------------ специальные геттеры/сеттеры внутренних классов
	// доступ к ним свободный
	public int getKindness() {
		return hlb.kindness;
	}

	public void setKindness(int kindness) {
		hlb.kindness = kindness;
	}
	
	public void addSkill(Skill sk) {
		this.skills.add(sk);
	}
	
	public void empowerFirstSkill() {
		skills.get(0).power++;
	}

	
}
